package mx.com.trupper.servicotruppercore.conrtoller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import mx.com.trupper.servicotruppercore.dto.OrdenDTO; 
import mx.com.trupper.servicotruppercore.service.OrdneService; 
@RequiredArgsConstructor
@RestController
public class OrdenController {

	private final OrdneService service;
	@GetMapping("/ordenes/{id}")
	public ResponseEntity<OrdenDTO> getId(@PathVariable Integer id) {
		OrdenDTO dto =service.getId(id);
		if(dto!=null) {
			return ResponseEntity.status(HttpStatus.OK).body(dto); 
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@PostMapping("/ordenes")
	public ResponseEntity<OrdenDTO> create(@RequestBody OrdenDTO dto) {
		if(dto!=null) {
			OrdenDTO dtoNuevo = service.create(dto);
			return ResponseEntity.status(HttpStatus.OK).body(dtoNuevo); 
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	@PutMapping("/ordenes")
	public ResponseEntity<OrdenDTO> modify(@RequestBody OrdenDTO dto) {
		if(dto!=null) {
			OrdenDTO dtoNuevo = service.modify(dto);
			return ResponseEntity.status(HttpStatus.OK).body(dtoNuevo); 
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
