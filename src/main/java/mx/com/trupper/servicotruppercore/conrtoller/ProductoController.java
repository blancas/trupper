package mx.com.trupper.servicotruppercore.conrtoller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import mx.com.trupper.servicotruppercore.dto.ProductoDTO;
import mx.com.trupper.servicotruppercore.service.ProductoService;
@RequiredArgsConstructor
@RestController
public class ProductoController {
	private final ProductoService service;
	@GetMapping("/productos/{id}")
	public ResponseEntity<ProductoDTO> getId(@PathVariable Integer id) {
		ProductoDTO dto =service.getId(id);
		if(dto!=null) {
			return ResponseEntity.status(HttpStatus.OK).body(dto); 
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	@PostMapping("/productos")
	public ResponseEntity<ProductoDTO> create(@RequestBody ProductoDTO dto) {
		if(dto!=null) {
			ProductoDTO dtoNuevo = service.create(dto);
			return ResponseEntity.status(HttpStatus.OK).body(dtoNuevo); 
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	@PutMapping("/productos")
	public ResponseEntity<ProductoDTO> modify(@RequestBody ProductoDTO dto) {
		if(dto!=null) {
			ProductoDTO dtoNuevo = service.modify(dto);
			return ResponseEntity.status(HttpStatus.OK).body(dtoNuevo); 
		}
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
}
