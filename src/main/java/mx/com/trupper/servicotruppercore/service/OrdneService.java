package mx.com.trupper.servicotruppercore.service;
import java.util.Date;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;
import mx.com.trupper.servicotruppercore.dto.OrdenDTO;
import mx.com.trupper.servicotruppercore.entity.Orden;
import mx.com.trupper.servicotruppercore.repository.OrdenRepository;
@Service
@RequiredArgsConstructor
public class OrdneService implements OrdenServiceI {

	private final OrdenRepository repository;
	private final ModelMapper mapper;
	@Override
	public OrdenDTO getId(Integer id) {
		if(id!=null) {
			Optional<Orden> optional= repository.findById(id);
			if(optional.isPresent()) {
				return mapper.map(optional.get(), OrdenDTO.class);  
			}
		}
		return null;
	}

	@Override
	public OrdenDTO create(OrdenDTO dto) {
		if(dto!=null) {  
			Orden entity= mapper.map(dto, Orden.class);
			entity.setFecha(new Date());
			return mapper.map(repository.save(entity), OrdenDTO.class);
		}
		return null;
	}

	@Override
	public OrdenDTO modify(OrdenDTO dto) {
		if(dto!=null) {
			if(dto.getOrdenId()!=null) {
				Optional<Orden> optional= repository.findById(dto.getOrdenId());
				if(optional.isPresent()) { 
					return mapper.map(repository.save(mapper.map(dto, Orden.class)), OrdenDTO.class);
				}
			}
		}
		return null;
	}

}
