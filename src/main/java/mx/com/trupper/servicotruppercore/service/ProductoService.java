package mx.com.trupper.servicotruppercore.service;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import mx.com.trupper.servicotruppercore.dto.ProductoDTO;
import mx.com.trupper.servicotruppercore.entity.Producto;
import mx.com.trupper.servicotruppercore.repository.ProductoRepository;
@Service
@RequiredArgsConstructor
public class ProductoService implements ProductoServiceI {

	private final ProductoRepository repository;
	private final ModelMapper mapper;
	@Override
	public ProductoDTO getId(Integer id) {
		if(id!=null) {
			Optional<Producto> optional= repository.findById(id);
			if(optional.isPresent()) {
				return mapper.map(optional.get(), ProductoDTO.class);  
			}
		}
		return null;
	}

	@Override
	public ProductoDTO create(ProductoDTO dto) {
		if(dto!=null) {
			if(dto.getProductoId()!=null) {
				Optional<Producto> optional= repository.findById(dto.getProductoId());
				if(optional.isEmpty()) { 
					return mapper.map(repository.save(mapper.map(dto, Producto.class)), ProductoDTO.class);
				}
			}
		}
		return null;
	}

	@Override
	public ProductoDTO modify(ProductoDTO dto) {
		if(dto!=null) {
			if(dto.getProductoId()!=null) {
				Optional<Producto> optional= repository.findById(dto.getProductoId());
				if(optional.isPresent()) { 
					return mapper.map(repository.save(mapper.map(dto, Producto.class)), ProductoDTO.class);
				}
			}
		}
		return null;
	}

}
