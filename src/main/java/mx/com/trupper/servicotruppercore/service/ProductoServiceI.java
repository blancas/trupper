package mx.com.trupper.servicotruppercore.service;

import mx.com.trupper.servicotruppercore.dto.ProductoDTO;

public interface ProductoServiceI {
	ProductoDTO getId(Integer id);
	ProductoDTO create(ProductoDTO dto);
	ProductoDTO modify(ProductoDTO dto);
	
}
