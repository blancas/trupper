package mx.com.trupper.servicotruppercore.service;

import mx.com.trupper.servicotruppercore.dto.OrdenDTO;

public interface OrdenServiceI {
	OrdenDTO getId(Integer id);
	OrdenDTO create(OrdenDTO dto);
	OrdenDTO modify(OrdenDTO dto);
	
}
