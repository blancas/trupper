package mx.com.trupper.servicotruppercore.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SucursalDTO {
	private Integer sucursalId; 
	private String nombre; 
	private List<OrdenDTO> ordenes;
}
