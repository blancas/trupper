package mx.com.trupper.servicotruppercore.dto;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter; 
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrdenDTO {
	private Integer ordenId; 
	private Date fecha; 
	private BigDecimal total; 
	private List<ProductoDTO> produtos;
}
