package mx.com.trupper.servicotruppercore.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductoDTO {
	private Integer productoId; 
	private String codigo; 
	private String descripcion;  
	private BigDecimal total;
}
