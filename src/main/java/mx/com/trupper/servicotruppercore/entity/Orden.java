package mx.com.trupper.servicotruppercore.entity;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Ordenes")
public class Orden {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ordenId;
	@Column(name = "fecha")
	private Date fecha;
	@Column(name = "total")
	private BigDecimal total;
	@OneToMany
    @JoinTable(name = "trupper_ordenes_produtor",
            joinColumns = @JoinColumn(name = "orden_id"),
            inverseJoinColumns = @JoinColumn(name = "producto_id")) 
	private List<Producto> produtos;
}
