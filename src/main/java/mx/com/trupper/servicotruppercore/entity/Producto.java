package mx.com.trupper.servicotruppercore.entity;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Produtos")
public class Producto {
	@Id
	private Integer productoId;
	@Column(name = "codigo")
	private String codigo;
	@Column(name = "descripcion")
	private String descripcion; 
	@Column(name = "total")
	private BigDecimal total;

}
