package mx.com.trupper.servicotruppercore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.com.trupper.servicotruppercore.entity.Orden;

@Repository
public interface OrdenRepository extends JpaRepository<Orden, Integer>{

}
